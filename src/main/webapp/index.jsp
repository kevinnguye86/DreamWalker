
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">

<!-- Latest compiled and minified CSS-->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"
	type="text/javascript"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-route.js"
	type="text/javascript"></script>

<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Loan Delivery Service</title>

<style>
.prodActive {
	background-color: #337ab7 !important;
}

p>.review-link {
	position: relative;
	top: 10%;
	cursor: pointer;
}

h1 {
	margin-bottom: 0;
}

.row {
	margin-bottom: 50px !important;
}
.carousel-inner img {
  margin: auto;
}
.carousel-inner > .item > img{
       width:300px;
       height:180px;
}
</style>

<script type="text/javascrip">
        var task = document.getElementById("task");
        
    </script>

</head>

<body>
	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="/">Loan Delivery Service</a>
			</div>

			<ul class="nav navbar-nav navbar-right">
				<li><a href="index.jsp"><i class="fa fa-home"></i> Home</a></li>
				<li><a href="bank.jsp"><i class="fa fa-shield"></i> Bank</a></li>
				<li><a href="smo.jsp"><i class="fa fa-shield"></i>SMO</a></li>
			</ul>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<h1>Welcome to Dream Walkers LDS</h1>
			<small class="text-muted">We will help you find your dream house</small>
		</div>
		
		
		<div id="myCarousel" class="carousel slide row" data-ride="carousel" style="width: 380px; margin: 0 auto">
	  	<!-- Indicators -->
		  <ol class="carousel-indicators">
		    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		    <li data-target="#myCarousel" data-slide-to="1"></li>
		    <li data-target="#myCarousel" data-slide-to="2"></li>
		    <li data-target="#myCarousel" data-slide-to="3"></li>
		  </ol>
		  
			<div class="carousel-inner" role="listbox">
		    	<div class="item active">
		    	<img src="images/bank.jpg" alt="Bank image"/>
		    		<div style="text-align: center">Are you a banker?</div>
		    	</div>
		    	<div class="item">
		    	<img src="images/money.jpg" alt="money image"/>
		    		<div style="text-align: center">Do you want to make money for your bank?</div>
		    	</div>
		    	<div class="item">
		    	<img src="images/moon.jpg" alt="moon image"/>
		    		<div style="text-align: center">We at DreamWalker are here to help!</div>
		    	</div>
		    	<div class="item">
		    	<img src="images/sleep_money.jpg" alt="sleep money image"/>
		    		<div style="text-align: center">We can help your bank earn money as you sleep!</div>
		    	</div>
		    </div>
		    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			  <span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			  <span class="sr-only">Next</span>
			</a>
		</div> <!-- End Carousel -->
		
		
		<div class="row">
			<div class="col-xs-3">
				<a class="btn btn-block btn-default btn-lg" href="bank.jsp">Bank</a>
				<a class="btn btn-block btn-default btn-lg" href="smo.jsp">SMO</a>
			</div>
			
			<div class="col-xs-1"></div>
			
			<div class="panel panel-success col-xs-4">
				<p class="panel-heading">Banker will be able to:</p>
				<ul>
					<li>Create and delete loans.</li>
					<li>View all loans.</li>
					<li>Create and delete MBS.</li>
					<li>Submit new MBS.</li>
				</ul>
			</div>
			
			<div class="panel panel-success col-xs-4">
				<p class="panel-heading">SMO will be able to:</p>
				<ul>
					<li>View all MBS.</li>
					<li>Approve or deny submitted MBS.</li>
				</ul>
			</div>
		</div>
	</div>
	
</body>

</html>