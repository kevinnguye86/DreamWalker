<%@ page import="com.ex.DAO.*, java.util.*, com.ex.pojos.*, com.ex.Util.*, java.text.*"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">

<!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
<!-- Data Table Script -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
  
  
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"
	type="text/javascript"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-route.js"
	type="text/javascript"></script>
<title>SMO Page</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.prodActive {
	background-color: #337ab7 !important;
}

p>.review-link {
	position: relative;
	top: 10%;
	cursor: pointer;
}

h1 {
	margin-bottom: 0;
}

.row {
	margin-bottom: 50px !important;
}
</style>


</head>

<body>
	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="/">Loan Delivery Service</a>
			</div>

			<ul class="nav navbar-nav navbar-right">
				<li><a href="index.jsp"><i class="fa fa-home"></i> Home</a></li>
				<li><a href="bank.jsp"><i class="fa fa-shield"></i> Bank</a></li>
				<li><a href="smo.jsp"><i class="fa fa-shield"></i>
						SMO</a></li>
			</ul>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<h1>Welcome to Dream Walkers LDS</h1>
			<small class="text-muted">We will help you find your dream
				house</small>
		</div>
		<div>
			<div class="row">
				<div class="col-xs-3">
					<form method="POST" action ="#">
					<button class="btn btn-block btn-default btn-lg" name="task"
						value="view_all_mbs">View all MBS</button>
					<button class="btn btn-block btn-default btn-lg" name="task"
						value="view_pending_mbs">View Pending MBS</button>
					<button class="btn btn-block btn-default btn-lg" name="task"
						value="view_approved_mbs">View Approved MBS</button>
					<button class="btn btn-block btn-default btn-lg" name="task"
						value="view_denied_mbs">View Denied MBS</button>
					</form>
				</div>
				
				<div class="panel panel-success col-xs-9"> 
					<%if(request.getParameter("approved") !=null){ %>
						<div class="alert alert-success" style="height: 35px;padding:0">
							<h4><strong>MBS was approved!</strong> MBS ID: <%=request.getParameter("approved")%></h4>
						</div>
					<%} %>
					
					<%if(request.getParameter("denied") !=null){ %>
						<div class="alert alert-success" style="height: 35px;padding:0">
							<h4><strong>MBS was denied!</strong> MBS ID: <%=request.getParameter("denied")%></h4>
						</div>
					<%} %>
					
					<%if (request.getParameter("task") != null){
						DecimalFormat df = new DecimalFormat("#,###.00");
						List<MBS> mbs = null;
						
						String s = "";
						s = request.getParameter("task");
						String status ="";
						switch (s){
							case "view_all_mbs":
								mbs = MBSDAO.AllMBS();
								if(mbs.size() == 0) status = "No MBS Available";
								break;
							case "view_pending_mbs":
								mbs = MBSDAO.StatusMBS("pending");
								if(mbs.size() == 0) status = "No MBS With Status Pending";
								break;
							case "view_approved_mbs":
								mbs = MBSDAO.StatusMBS("approved");
								if(mbs.size() == 0) status = "No MBS With Status Approved";
								break;
							case "view_denied_mbs":
								mbs = MBSDAO.StatusMBS("denied");
								if(mbs.size() == 0) status = "No MBS With Status Denied";
								break;
							
							default:
								
								break;
									
						}
						
						SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy");
						
						if(mbs.size() == 0){
					%>
						<div class="alert alert-info"><%=status %> </div>
					<%} %>
						<table id="all_mbs_table" class="table table-hover">
						<thead>
							<tr>
							<th width="90px" bgcolor="#ABEBC6">MBS ID</th>
							<th width="140px" bgcolor="#ABEBC6">Bank</th>
							<th width="100px" bgcolor="#ABEBC6">Sum Amount</th>
							<th width="120px" bgcolor="#ABEBC6">Average Rate</th>
							<th width="130px" bgcolor="#ABEBC6">SMO</th>
							<th width="70px" bgcolor="#ABEBC6">Status</th>
							<th width="160px" bgcolor="#ABEBC6"></th>
							</tr>
						</thead>
						<tbody>
						
						<%for(MBS m: mbs ){
							String stat = m.getStatus();
						%>
							
							<tr>
								<td><%=m.getMbs_id() %></td>
								<td><%=m.getBank_name() %></td>
								<td><%=df.format(m.getSum_amount()) %></td>
								<td><%=Math.round(m.getAvg_rate()*100.0)/100.0 %></td>
								<td><%=m.getSMO() %></td>
								<%switch(stat){ 
									case "denied":
								%>
										<td><span class="glyphicon glyphicon-remove alert-danger"></span> denied</td>
								<%		break;
									case "approved":
								 %>
										<td><span class="glyphicon glyphicon-ok alert-success"></span> approved</td>
								<%		break;
									case "pending":
								 %>
								 		<td><span class="glyphicon glyphicon-time alert-warning"></span> pending</td>
								<%		break;
									default:
										break;
								} %>
								
								<td> <button class="btn btn-primary btn-md" data-toggle="modal" data-target="#view_mbs_div" name="loans" 
								value="<%=m.getMbs_id()%>" onclick="view_mbs_ajax(<%=m.getMbs_id()%>)"><span class="glyphicon glyphicon-list-alt"></span> Details</button></td>
							</tr>
								
						<%}%>
						</tbody>
						</table>
					
					<%}%>
				</div>
			</div>
		</div>
	</div>
	
	<div id="view_mbs_div" class="modal fade" role="dialog">
  		<div class="modal-dialog">

    	<!-- Modal content-->
		<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
        		<h4 class="modal-title">MBS Details</h4>
      		</div>
      		<div class="modal-body">
		        <form method="POST" action="mbs_service">
				<table class="table table-hover">
					<tr><td>MBS ID: </td><td id="view_mbs_id"></td>
					</tr>
					<tr><td>Number of Loans: </td><td id="view_mbs_num_loans"></td>
					</tr>
					<tr><td>Sum Amount: </td><td id="view_mbs_sum_amount"></td>
					</tr>
					<tr><td>Average Interest Rate: </td><td id="view_mbs_avg_rate"></td>
					</tr>
					<tr><td>Bank: </td><td id="view_mbs_bank_name"></td>
					</tr>
					<tr><td>SMO: </td><td id="view_mbs_smo"></td>
					</tr>
					<tr><td>Status: </td><td id="view_mbs_status"></td>
					</tr>
					
				</table>
				<p>List of loans:</p>
				<div class="pre-scrollable" style="max-height:200px; min-height:225px;">
					<table class="table table-hover">
					<thead>
						<tr>
						<th width="90px" bgcolor="#FEF9E7">ID</th>
						<th width="140px" bgcolor="#FEF9E7">Amount</th>
						<th width="100px" bgcolor="#FEF9E7">Rate</th>
						<th width="150px" bgcolor="#FEF9E7">Bank</th>
						<th width="150px" bgcolor="#FEF9E7">Created Date</th>
						<!--th width="70px" bgcolor="#FEF9E7">MBS ID</th>
						<th width="160px" bgcolor="#FEF9E7"></th-->
						</tr>
					</thead>
					<tbody id="mbs_view_loan_list">
					</tbody>
					</table>
				</div>
				<button id="approve_button" class="btn btn-primary btn-md alert alert-success" name="approve" value="">
					<span class="glyphicon glyphicon-ok"></span> Approve</button>
				<br/><br/>
				<textarea class="form-control" id="denial_text" name="reason" placeholder=" If deny, please enter reason for denial!" onkeyup="deny_button()"></textarea>
				<h6 class="pull-right" id="count_message"></h6>
				
				<button class="btn btn-primary btn-md alert alert-danger" id="denial_button" title="Enter reason before you deny!" disabled name="deny" value="">
					<span class="glyphicon glyphicon-remove"></span> Deny</button>
				
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
		</div>
    </div> <!-- End View MBS Modal -->

	<script src="script.js"></script>
</body>

</html>