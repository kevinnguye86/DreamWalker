<%@ page import="com.ex.DAO.*, java.util.*, com.ex.pojos.*, com.ex.Util.*, java.text.*"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">

<!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 
<!-- Data Table Script -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
  
  
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"
	type="text/javascript"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-route.js"
	type="text/javascript"></script> 
	
<title>Bank Page</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.prodActive {
	background-color: #337ab7 !important;
}

p>.review-link {
	position: relative;
	top: 10%;
	cursor: pointer;
}

h1 {
	margin-bottom: 0;
}

.row {
	margin-bottom: 50px !important;
}
</style>


</head>

<body>
	<%! List<Bank> banks = BankDAO.getBanks(); DecimalFormat df = new DecimalFormat("#,###.00");%>
	
	
	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="/">Loan Delivery Service</a>
			</div>

			<ul class="nav navbar-nav navbar-right">
				<li><a href="index.jsp"><i class="fa fa-home"></i> Home</a></li>
				<li><a href="bank.jsp"><i class="fa fa-shield"></i> Bank</a></li>
				<li><a href="smo.jsp"><i class="fa fa-shield"></i>
						SMO</a></li>
			</ul>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<h1>Welcome to Dream Walkers LDS</h1>
			<small class="text-muted">We will help you find your dream
				house</small>
		</div>
		
		<div class="row">
			<div class="col-xs-3">
				<form method="POST" action ="bank.jsp">
				<button class="btn btn-block btn-default btn-lg" name="task"
					value="new_loan">Create A New Loan</button>
				<button class="btn btn-block btn-default btn-lg" name="task"
					value="view_loans">View All Loans</button>
				<button class="btn btn-block btn-default btn-lg" name="task"
					value="quick_submit_MBS">Quick Submit MBS</button>
				<button class="btn btn-block btn-default btn-lg" name="task"
					value="manual_submit_MBS">Manual Submit MBS</button>
				<button class="btn btn-block btn-default btn-lg" name="task"
					value="denied_MBS">Denied MBS</button>
				</form>
			</div>
			<div class="panel panel-success col-xs-9">
				
				<!-- Create New Loan -->
				<%if ((request.getParameter("task") != null && request.getParameter("task").equals("new_loan"))
						|| request.getParameter("created") != null) {
					if(request.getParameter("created") != null) {
					
				%> <div class="alert alert-success" style="height: 35px;padding:0"><h4><strong>New Loan Was Added!</strong> Loan ID: <%=request.getParameter("created")%></h4></div>
				<%}%>
				<div class="panel-heading" style="height: 35px;padding:0">
					<h3 id="task">Create new loan</h3>
				</div>
				<div class="panel-body">
					<form method="POST" action="loan_service">
					<table class="table table-hover">
						<tr>
							<td>Amount: </td><td><input class="form-control" type="number" min="50000" max="3000000" id="new_loan_amount" name="amount" onkeyup="validate()" required/></td>
						</tr>
						<tr>
							<td>Interest Rate: </td><td><input class="form-control" type="number" min="0.25" max="25" step="0.25" id="new_loan_rate" name="interest_rate" onkeyup="validate()" required/></td>
						</tr>
						<tr>
							<td>Bank: </td>
							<td>
								<select id="new_loan_banks" class="form-control" style="width: 150px" name="bank">
									<%
										List<Bank> banks = BankDAO.getBanks();
										for(Bank b: banks){
									%>
										<option id="new_loan_bank<%=b.getBank_id()%>" value="<%= b.getBank_id()%>"><%= b.getName() %></option>
									
									<%} %>
								</select>
							</td>
						</tr>
						<tr><td></td></tr>
						
					</table>
					<button id="new_loan_submit" class="btn btn-primary btn-md alert alert-success" name="submit" value="create_loan">
						<span class="glyphicon glyphicon-save"></span> Add Loan</button>
					</form>
				</div>
				<%}%>
				
				<!-- End Create New Loan -->
				
				
				<!-- View All Loans -->
				<%if ((request.getParameter("task") != null && request.getParameter("task").equals("view_loans"))
					 || request.getParameter("edited") != null
					 || request.getParameter("deleted") !=null) {
					DecimalFormat df = new DecimalFormat("#,###.00");
					SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy");
					
					List<Loan> loans = LoanDAO.getLoans();
					List<MBS> denied_mbs = MBSDAO.StatusMBS("denied");
					List<Integer> denied_id = new ArrayList<Integer>();
					for(MBS m : denied_mbs){
						denied_id.add(m.getMbs_id());
					}
					
					if(request.getParameter("edited") != null) {
					
						%> <div class="alert alert-success" style="height: 35px;padding:0"><h4><strong>Loan was edited!</strong> Loan ID: <%=request.getParameter("edited")%></h4></div>
					<%}
					
					if(request.getParameter("deleted") != null) {
					
						%> <div class="alert alert-success" style="height: 35px;padding:0"><h4><strong>Loan was deleted!</strong> Loan ID: <%=request.getParameter("deleted")%></h4></div>
					<%}%>
				
				<div class="panel-heading" style="height: 35px;padding:0">
					<h3 id="task">List of loans</h3>
				</div>
				
					<table id="all_loans_table" class="table table-hover">
					<thead>
					<tr>
						<th width="90px" bgcolor="#ABEBC6">ID</th>
						<th width="140px" bgcolor="#ABEBC6">Amount</th>
						<th width="100px" bgcolor="#ABEBC6">Rate</th>
						<th width="150px" bgcolor="#ABEBC6">Bank</th>
						<th width="150px" bgcolor="#ABEBC6">Created Date</th>
						<th width="70px" bgcolor="#ABEBC6">MBS ID</th>
						<th width="160px" bgcolor="#ABEBC6"></th>
					</tr>
					</thead>
					<tbody>
					<% for(int i = 0; i < loans.size(); i++){ 
						int id = loans.get(i).getLoan_id();%>
					<tr>
						<td><%= id %></td>
						<td><%= df.format(loans.get(i).getAmount()) %></td>
						<td><%= Math.round(loans.get(i).getRate()*100.0)/100.0 %></td>
						<td><%= loans.get(i).getBank_name() %></td>
						<td><%= sdf.format(loans.get(i).getCreated_date()) %></td>
						<td><%= loans.get(i).getMbs_id() %></td>
						<td>
							<% if(loans.get(i).getMbs_id()==0 || denied_id.contains(loans.get(i).getMbs_id())){%>
								<button class="btn btn-primary btn-md alert-info" name="edit" value="<%=id%>" data-toggle="modal" data-target="#edit_loan_div" onclick="edit_loan_ajax(<%=id%>)"><span class="glyphicon glyphicon-edit"></span></button>
								<button class="btn btn-primary btn-md alert-danger" name="delete" value="<%=id%>" data-toggle="modal" data-target="#delete_loan_div" onclick="delete_loan_ajax(<%=id%>)"><span class="glyphicon glyphicon-trash"></span></button>
							<%}else{ %>
								<button class="btn btn-primary btn-md alert-info" data-toggle="tooltip" data-placement="top" title="The MBS this loan belongs to is not denied" 
										disabled name="edit" value="<%=id%>"><span class="glyphicon glyphicon-edit"></span></button>
								<button class="btn btn-primary btn-md alert-danger" data-toggle="tooltip" data-placement="top" title="The MBS this loan belongs to is not denied" 
										disabled name="delete" value="<%=id%>"><span class="glyphicon glyphicon-trash"></span></button>
									
								<% }%>
								
						</td>
					</tr>
						
					<%} %>
					</tbody>
					</table>
					
				<%}%>
				<!-- End View All Loans -->
			
			
			
				<!-- begin Quick submit MBS -->
			
				<%if (request.getParameter("task") != null && request.getParameter("task").equals("quick_submit_MBS")
						|| request.getParameter("quick_mbs_bank") != null
						|| request.getParameter("quick_mbs_submitted") != null){
					DecimalFormat df = new DecimalFormat("#,###.00");
					SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy");
					
					
					%><form method="POST" action="bank.jsp"><%
					for(Bank b: banks){%>
					
						<button class="btn btn-primary btn-md" name="quick_mbs_bank" value="<%= b.getBank_id()%>"><%= b.getName() %></button>
												 
					<%} %>
					</form>
					<%
					if(request.getParameter("quick_mbs_submitted") != null){
						%>
						<div class="alert alert-success" style="height: 35px;padding:0"><h4><strong>MBS was submitted!</strong> MBS ID: <%=request.getParameter("quick_mbs_submitted")%></h4></div>
						<%
					}	
					if( request.getParameter("quick_mbs_bank") != null )
					{
						
						int bank_id = Integer.parseInt(request.getParameter("quick_mbs_bank"));
						List<Loan> loans = LoanDAO.BanksLoansForMBS(bank_id);
						%>
						<div class="pre-scrollable" style="max-height:225px; min-height:225px;">
						<table class="table table-hover">
						<tr>
							<td width="90px" bgcolor="#ABEBC6">ID</td>
							<td width="150px" bgcolor="#ABEBC6">Amount</td>
							<td width="100px" bgcolor="#ABEBC6">Rate</td>
							<td width="150px" bgcolor="#ABEBC6">Bank</td>
							<td width="150px" bgcolor="#ABEBC6">Created Date</td>
							
						</tr>
						<%
						if(loans.size() == 0){%>
							<tr>
							<td></td>
							<td>No Loan Available</td>
							</tr>
						
						<%}
						
						double sum=0;
						double avg=0;
						for(Loan l : loans){%>
							
							<tr>
								<td><%= l.getLoan_id() %></td>
								<td><%= df.format(l.getAmount()) %></td>
								<td><%= l.getRate() %></td>
								<td><%= l.getBank_name() %></td>
								<td><%= sdf.format(l.getCreated_date()) %></td>
								
							</tr>	
						
						<% sum+=l.getAmount();
						   avg+=l.getRate();
						}
						
						if(loans.size() > 0)
							avg = avg/loans.size();%>
						</table>
						</div>
						<br/><br/>
						<form method="POST" action="mbs_service">
						<table class="table table-hover">
							<tr><td>Number of Loans: </td><td><%=loans.size() %>
								<input hidden="hidden" type="text" name = "num_loans" value = "<%=loans.size() %>"></td></tr>
							<tr><td>Sum Amount: </td><td><%=df.format(sum) %>
								<input hidden="hidden" type="text" name = "sum" value = "<%=sum %>"></td></tr>
							<tr><td>Average Rate:</td><td><%=Math.round(avg*100.0)/100.0%>
								<input hidden="hidden" type="text" name = "avg" value = "<%=avg%>"></td></tr>
								
							<tr>
								<td>SMO:</td>
							
								<td> 
								<select class="form-control" name="SMO">
									<option value="Fannie Mae">Fannie Mae</option>
									<option value="Freddie Mac">Freddie Mac</option>
									<option value="Wells Fargo">Wells Fargo</option>		
								</select>
								</td>
							</tr>
							
								<tr><td><% if(sum >= 3000000){%><button class="btn btn-primary btn-md" name="quick_submit_mbs" value="<%=bank_id%>">Submit MBS</button></td></tr>
							<%}else{%>
								<tr><td><button class="btn btn-primary btn-md" data-toggle="tooltip" data-placement="top" title="Sum Amount must be at least 3 million dollars!" 
										disabled name="quick_submit_mbs" value="<%=bank_id%>">Submit MBS</button></td></tr>
							<%} %>
						</table>
						</form>
					<%}
					else{%>
						<div class="alert alert-info">Please Choose A Bank</div>
					<%} %>
				<%} %>
				
				<!-- end Quick Submit MBS -->
			
			
				<!-- Begin Manual submit MBS -->
			
				<%if (request.getParameter("task") != null && request.getParameter("task").equals("manual_submit_MBS")
						|| request.getParameter("manual_mbs_bank") != null
						|| request.getParameter("manual_mbs_submitted") != null){
					
					SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy");
					
					for(Bank b: banks){%>
					
						<button class="btn btn-primary btn-md" name="manual_mbs_bank" value="<%= b.getBank_id()%>" 
							onclick="manual_mbs_ajax(<%=b.getBank_id()%>)"><%= b.getName() %></button>
												 
					<%} %>
					
					<%if(request.getParameter("manual_mbs_submitted") != null){%>
						<div class="alert alert-success" style="height: 35px;padding:0"><h4><strong>MBS was submitted!</strong> MBS ID: <%=request.getParameter("manual_mbs_submitted")%></h4></div>
					<%}%>
					
						<form method="POST" action="mbs_service">
						<div class="pre-scrollable" style="max-height:225px; min-height:225px;">
						<table class="table table-hover">
						<thead>
							<tr>
							<th width="30px" bgcolor="#ABEBC6"></th>
							<th width="90px" bgcolor="#ABEBC6">ID</th>
							<th width="150px" bgcolor="#ABEBC6">Amount</th>
							<th width="100px" bgcolor="#ABEBC6">Rate</th>
							<th width="150px" bgcolor="#ABEBC6">Bank</th>
							<th width="150px" bgcolor="#ABEBC6">Created Date</th>
							</tr>
						</thead>
						<tbody id="manual_loans">
						
						</tbody>
						</table>
						</div>
						<br/><br/>
						
						<table class="table table-hover">
							<tr><td>Number of Loans: </td><td>
							<input class="form-control" readonly id="manual_mbs_num_loans" type="text" name="manual_mbs_num_loans" value=""/></td></tr>
							<tr><td>Sum Amount: </td><td>
							<input class="form-control" readonly id="manual_mbs_sum_amount" type="text" name="manual_mbs_sum_amount" value=""/></td></tr>
							<tr><td>Average rate:</td><td>
							<input class="form-control" readonly id="manual_mbs_avg_rate" type="text" name="manual_mbs_avg_rate" value=""/></td></tr>
								
							<tr>
								<td>SMO:</td>
							
								<td> 
								<select class="form-control" name="SMO">
									<option value="Fannie Mae">Fannie Mae</option>
									<option value="Freddie Mac">Freddie Mac</option>
									<option value="Wells Fargo">Wells Fargo</option>		
								</select>
								</td>
							</tr>
							
							<tr><td>
								<button id="manual_submit" class="btn btn-primary btn-md" data-toggle="tooltip" data-placement="top" 
									disabled name="manual_submit_mbs" value="">Submit MBS</button>
							</td></tr>
							
						</table>
						
						
						
						
						</form>
					
					
				<%} %> <!-- End Manual Submit MBS -->
			
				<!-- Begin denied mbs -->
				
				<%if(request.getParameter("task") != null && request.getParameter("task").equals("denied_MBS")
						|| request.getParameter("denied_bank") != null
						|| request.getParameter("denied_mbs_submitted") != null
						|| request.getParameter("denied_mbs_save") != null){
					SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy");
					
				%>
					<form method="POST" action="bank.jsp">
				
				<%	for(Bank b: banks){%>
						
						<button class="btn btn-primary btn-md" name="denied_bank" value="<%= b.getBank_id()%>"><%= b.getName() %></button>
									
					<%} %>
					</form>
					<%if(request.getParameter("denied_mbs_submitted") != null){%>
						<div class="alert alert-success" style="height: 35px;padding:0"><h4><strong>MBS was submitted!</strong> MBS ID: <%=request.getParameter("denied_mbs_submitted")%></h4></div>
					<%}%>
					
					<%if(request.getParameter("denied_mbs_save") != null){%>
						<div class="alert alert-success" style="height: 35px;padding:0"><h4><strong>MBS was saved!</strong> MBS ID: <%=request.getParameter("denied_mbs_save")%></h4></div>
					<%}%>
					
					<div class="pre-scrollable" style="max-height:150px; min-height:225px;">
					<table class="table table-hover">
					<thead>
						<tr>
						<th width="90px" bgcolor="#ABEBC6">MBS ID</th>
						<th width="140px" bgcolor="#ABEBC6">Bank</th>
						<th width="100px" bgcolor="#ABEBC6">Sum Amount</th>
						<th width="120px" bgcolor="#ABEBC6">Average Rate</th>
						<th width="130px" bgcolor="#ABEBC6">SMO</th>
						<th width="70px" bgcolor="#ABEBC6">Status</th>
						<th width="160px" bgcolor="#ABEBC6"></th>
						</tr>
					</thead>
					<tbody>
					<%if(request.getParameter("denied_bank") != null){ 
						List<MBS> denied_mbs = MBSDAO.BankStatusMBS(Integer.parseInt(request.getParameter("denied_bank")), "denied");
						
						if(denied_mbs.size() == 0){
							Bank b = BankDAO.getBank(Integer.parseInt(request.getParameter("denied_bank")));
					%>
						<tr><td></td><td>No Denied MBS in <%=b.getName() %></td></tr>
					<%}
					
						for(MBS m: denied_mbs ){%>
						
						<tr>
							<td><%=m.getMbs_id() %></td>
							<td><%=m.getBank_name() %></td>
							<td><%=df.format(m.getSum_amount()) %></td>
							<td><%=Math.round(m.getAvg_rate()*100.0)/100.0 %></td>
							<td><%=m.getSMO() %></td>
							<td><%=m.getStatus() %></td>
							
							<td> <button class="btn btn-primary btn-md" name="denied_mbs_fill" data-toggle="collapse" data-target="#denied_row"
							value="<%=m.getMbs_id()%>" onclick="denied_mbs_json(<%=m.getMbs_id()%>)">View Details</button></td>
						</tr>
							
						<%}%>
					<%} %>
					</tbody>
					</table>
					</div>
						
				    
				<% } %>
				
				<!-- End denied mbs -->
			
			</div> <!-- End Content -->
			
		</div> <!-- End content Row -->
		
		<div id="denied_row" class="row collapse">
			<div class="col-xs-3">
				<form method="POST" action="mbs_service">
                <table class="table table-hover">
                	<tr><td>MBS ID: </td><td>
                	<input class="form-control" readonly id="denied_mbs_id" type="text" name="denied_mbs_id" value=""/></td></tr>
                    <tr><td>Number of Loans: </td><td>
                    <input class="form-control" readonly id="denied_mbs_num_loans" type="text" name="denied_mbs_num_loans" value=""/></td></tr>
                    <tr><td>Sum Amount: </td><td>
                    <input class="form-control" readonly id="denied-mbs-sum-amount" type="text" name="denied-mbs-sum-amount" value=""/>
                    <input class="form-control hidden" id="denied_mbs_sum_amount" type="text" name="denied_mbs_sum_amount" value=""/></td></tr>
                    <tr><td>Average rate:</td><td>
                    <input class="form-control" readonly id="denied-mbs-avg-rate" type="text" name="denied-mbs-avg-rate" value=""/>
                    <input class="form-control hidden" id="denied_mbs_avg_rate" type="text" name="denied_mbs_avg_rate" value=""/></td></tr>
                        
                    <tr><td>SMO:</td>
                    
                        <td> 
                        <select class="form-control" name="SMO">
                            <option id="Fannie Mae" value="Fannie Mae">Fannie Mae</option>
                            <option id="Freddie Mac" value="Freddie Mac">Freddie Mac</option>
                            <option id="Wells Fargo" value="Wells Fargo">Wells Fargo</option>		
                        </select>
                        </td>
                    </tr>
                </table>    
                <p>Denied For:</p>
                <textarea readonly class="form-control" id="denied_mbs_reason" name="denied_mbs_reason" value=""></textarea>
                <br/>
                <button id="denied_mbs_submit" class="btn btn-primary btn-md" 
                	disabled name="denied_mbs_submit" value="">Submit MBS</button>
                <button id="denied_mbs_save" class="btn btn-primary btn-md"  
                	name="denied_mbs_save" value="">Save</button>
                	
                <div id="hidden_loans"></div>
				</form>
            </div> <!-- End div mbs info column -->

            <div class="col-xs-4">
            	<h4 class="alert alert-success">List of added loans:</h4>
                <div class="pre-scrollable" style="max-height:300px; min-height:300px;">
                
                <table class="table table-hover">
                <thead>
                    <tr>
                    <th width="50px" bgcolor="#ABEBC6">ID</th>
                    <th width="150px" bgcolor="#ABEBC6">Amount</th>
                    <th width="100px" bgcolor="#ABEBC6">Rate</th>
                    <th width="100px" bgcolor="#ABEBC6"></th>
                    </tr>
                </thead>
                <tbody id="added_loans">
                
                </tbody>
                </table>
                </div>
            </div> <!-- End div added loan list column -->

            <div class="col-xs-1">
                
            </div> <!-- End div blank column -->

            <div class="col-xs-4">
            	<h4 class="alert alert-success">List of available loans:</h4>
                <div class="pre-scrollable" style="max-height:300px; min-height:300px;">
                
                <table class="table table-hover">
                <thead>
                    <tr>
                    <th width="50px" bgcolor="#ABEBC6">ID</th>
                    <th width="150px" bgcolor="#ABEBC6">Amount</th>
                    <th width="100px" bgcolor="#ABEBC6">Rate</th>
                    <th width="100px" bgcolor="#ABEBC6"></th>
                    </tr>
                </thead>
                <tbody id="available_loans">
                
                </tbody>
                </table>
                </div>
            </div> <!-- End div available loan column -->

        </div> <!-- End denied row -->
		
	</div> <!-- End Outer Container -->
	
	<div id="edit_loan_div" class="modal fade" role="dialog">
  		<div class="modal-dialog">

    	<!-- Modal content-->
		<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
        		<h4 class="modal-title">Edit Loan Information</h4>
      		</div>
      		<div class="modal-body">
		        <form method="POST" action="loan_service">
				<table>
					<tr>
						<td>Loan ID: </td><td id="edit_loan_id"></td>
					</tr>
					<tr>
						<td>Amount: </td><td><input class="form-control" id="edit_loan_amount" type="number" min="50000" max="3000000" name="amount" value="" onkeyup="validate_edit()" required/></td>
					</tr>
					<tr>
						<td>Interest Rate: </td><td><input class="form-control" id="edit_loan_rate" type="number" min="0.25" max="25" step="0.25" onkeyup="validate_edit()" name="interest_rate" value="" required/></td>
					</tr>
					<tr>
						<td>Bank: </td>
						<td>
							<select id="modal_edit_loan_bank" class="form-control" style="width: 150px" name="bank">
								<%
									List<Bank> banks = BankDAO.getBanks();
									for(Bank b: banks){
								%>
									<option id="edit_loan_bank<%=b.getBank_id()%>" value="<%= b.getBank_id()%>"><%= b.getName() %></option>
								
								<%} %>
							</select>
						</td>
					</tr>
					
					<tr><td><button class="btn btn-primary btn-md alert-success" id="commit_edit_loan" name="commit_edit_loan" value="">
						<span class="glyphicon glyphicon-floppy-disk"></span> Save</button></td></tr>
					
				</table>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
		</div>
    </div> <!-- End Edit Loan Modal -->
    
    <div id="delete_loan_div" class="modal fade" role="dialog">
  		<div class="modal-dialog">

    	<!-- Modal content-->
		<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
        		<h4 class="modal-title">Cautions</h4>
      		</div>
      		<div class="modal-body">
      			<div class="alert alert-danger">Are you sure you want to delete this loan?</div>
		        <form method="POST" action="loan_service">
				<table>
					<tr>
						<td>Loan ID: </td><td id="delete_loan_id"></td>
					</tr>
					<tr>
						<td>Amount: </td><td id="delete_loan_amount"></td>
					</tr>
					<tr>
						<td>Interest Rate: </td><td id="delete_loan_rate"></td>
					</tr>
					<tr>
						<td>Bank: </td><td id="delete_loan_bank"></td>
					</tr>
				</table>
				<div class="alert alert-warning">
					<button class="btn btn-primary btn-md alert-danger" id="commit_delete_loan" name="commit_delete_loan" value="">
						<span class="glyphicon glyphicon-trash"></span> Delete</button>
					<button class="btn btn-primary btn-md" data-dismiss="modal">Cancel</button>
				</div>
				</form>	
					
				
				
			</div>
			<div class="modal-footer">
				
			</div>
		</div>
		</div>
    </div> <!-- End Delete Loan Modal -->
	
	<script src="script.js"></script>
	
</body>

</html>