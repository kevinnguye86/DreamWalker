$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
});

$(document).ready(function() {
  $('#all_loans_table').DataTable();
	$('#all_mbs_table').DataTable();
});

function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}
/*
var validate = function(){
	var amount = document.getElementById("new_loan_amount");
	var rate = document.getElementById("new_loan_rate");

	//if(amount.value < 50000 || amount.value > 3000000 || rate.value<=0.0 || rate.value>25.0 || rate.value == "" || amount.value == "" || isNaN(amount.value) || isNaN(rate.value) || (rate.value/0.25)%1 != 0)
	if(rate.value == "" || amount.value == "")
	{
		document.getElementById("new_loan_submit").setAttribute("disabled", "true");	
	}else{
		document.getElementById("new_loan_submit").removeAttribute("disabled");
	}
	
};

var validate_edit = function(){
	var amount = document.getElementById("edit_loan_amount");
	var rate = document.getElementById("edit_loan_rate");

	//if(amount.value < 50000 || amount.value > 3000000 || rate.value<=0.0 || rate.value>25.0 || rate.value == "" || amount.value == "" || isNaN(amount.value) || isNaN(rate.value) || (rate.value/0.25)%1 != 0)
	if(rate.value == "" || amount.value == "")
	{
		document.getElementById("commit_edit_loan").setAttribute("disabled", "true");	
	}else{
		document.getElementById("commit_edit_loan").removeAttribute("disabled");
	}
	
};*/

var text_max = 1500;
$('#count_message').html(text_max + ' remaining');

$('#denial_text').keyup(function() {
  var text_length = $('#denial_text').val().length;
  var text_remaining = text_max - text_length;
  
  $('#count_message').html(text_remaining + ' remaining');
});

function deny_button(){
	var d_text = document.getElementById("denial_text");
	var d_button = document.getElementById("denial_button");
	if(d_text.value.length == 0){
		d_button.setAttribute("disabled", "true");
	}
	else{
		d_button.removeAttribute("disabled");
	}
}

var xhr;

var clear_edit_loan = function(){
	document.getElementById("edit_loan_id").innerHTML = "";
	document.getElementById("edit_loan_amount").value = "";
	document.getElementById("edit_loan_rate").value = "";
	
};

var edit_loan_ajax = function(loan_id){
	clear_edit_loan();
	if(XMLHttpRequest){
	  xhr = new XMLHttpRequest();
	}else{
	  xhr  = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xhr.onreadystatechange = function(){
	  if(xhr.readyState != 4) return;
	  
	  if(xhr.status == 200){
	    var res = JSON.parse(xhr.responseText);
	    document.getElementById("edit_loan_id").innerHTML = res.loan_id;
	    document.getElementById("edit_loan_amount").value = res.amount;
	    document.getElementById("edit_loan_rate").value = res.rate;
	    document.getElementById("modal_edit_loan_bank").selectedIndex = res.bank_id -1;
	    
	    document.getElementById("commit_edit_loan").value = res.loan_id;
	    
	  }else{
	    console.error("An Error");
	    
	  }
	}
	
	xhr.open("GET", "http://localhost:7001/DreamWalker/loan_service?get_loan_json=" + loan_id, true);
	
	xhr.send();
};

var  delete_loan_ajax = function(loan_id){
	
	if(XMLHttpRequest){
	  xhr = new XMLHttpRequest();
	}else{
	  xhr  = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xhr.onreadystatechange = function(){
	  if(xhr.readyState != 4) return;
	  
	  if(xhr.status == 200){
	    var res = JSON.parse(xhr.responseText);
	    document.getElementById("delete_loan_id").innerHTML = res.loan_id;
	    document.getElementById("delete_loan_amount").innerHTML = numberWithCommas(res.amount);
	    document.getElementById("delete_loan_rate").innerHTML = Math.round(res.rate * 100) /100;
	    document.getElementById("delete_loan_bank").innerHTML = res.bank_name;
	    
	    document.getElementById("commit_delete_loan").value = res.loan_id;
	    
	  }else{
	    console.error("An Error");
	    
	  }
	}
	
	xhr.open("GET", "http://localhost:7001/DreamWalker/loan_service?get_loan_json=" + loan_id, true);
	
	xhr.send();
};

var clear_view_mbs = function(){
		document.getElementById("approve_button").setAttribute('class', "hidden btn btn-primary btn-md alert alert-success");
		document.getElementById("denial_button").setAttribute('class', "hidden btn btn-primary btn-md alert alert-danger");
		document.getElementById("count_message").setAttribute('class', "hidden pull-right");

		document.getElementById("view_mbs_id").innerHTML = "";
		document.getElementById("view_mbs_num_loans").innerHTML = "";
		document.getElementById("view_mbs_sum_amount").innerHTML = "";
		document.getElementById("view_mbs_avg_rate").innerHTML = "";
		document.getElementById("view_mbs_bank_name").innerHTML = "";
		document.getElementById("view_mbs_smo").innerHTML = "";

		document.getElementById("view_mbs_status").innerHTML = "";
		document.getElementById("mbs_view_loan_list").innerHTML = "";
};

var  view_mbs_ajax = function(mbs_id){
	clear_view_mbs();
	if(XMLHttpRequest){
	  xhr = new XMLHttpRequest();
	}else{
	  xhr  = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xhr.onreadystatechange = function(){
	  if(xhr.readyState != 4) return;
	  
	  if(xhr.status == 200){
	    var res = JSON.parse(xhr.responseText);

			if(res.status == "approved" || res.status == "open" || res.status == "denied"){
				document.getElementById("approve_button").setAttribute('class', "hidden btn btn-primary btn-md alert alert-success");
				document.getElementById("denial_button").setAttribute('class', "hidden btn btn-primary btn-md alert alert-danger");
				document.getElementById("count_message").setAttribute('class', "hidden pull-right");
				if(res.denial_reason != null){
					document.getElementById("denial_text").setAttribute('class', "form-control");
					document.getElementById("denial_text").value = res.denial_reason;
				}
				else{
					document.getElementById("denial_text").setAttribute('class', "hidden form-control");
					
				}
			}
			else{
				document.getElementById("approve_button").setAttribute('class', "btn btn-primary btn-md alert alert-success");
				document.getElementById("denial_button").setAttribute('class', "btn btn-primary btn-md alert alert-danger");
				document.getElementById("denial_text").setAttribute('class', "form-control");
				document.getElementById("count_message").setAttribute('class', "pull-right");
				document.getElementById("denial_text").value = "";

				document.getElementById("approve_button").value = res.mbs_id;
				document.getElementById("denial_button").value = res.mbs_id;

			}

	    document.getElementById("view_mbs_id").innerHTML = res.mbs_id;
	    document.getElementById("view_mbs_num_loans").innerHTML = res.num_loans;
	    document.getElementById("view_mbs_sum_amount").innerHTML = numberWithCommas(res.sum_amount);
	    document.getElementById("view_mbs_avg_rate").innerHTML = Math.round(res.avg_rate*100)/100;
	    document.getElementById("view_mbs_bank_name").innerHTML = res.bank_name;
			document.getElementById("view_mbs_smo").innerHTML = res.SMO;

			var result = "";
			var stat = res.status;
			switch(stat){
				case "denied":
					result = "<span class='glyphicon glyphicon-remove alert-danger'></span> " + res.status;
					break;
				case "approved":
					result = "<span class='glyphicon glyphicon-ok alert-success'></span> " + res.status;
					break;
				case "pending":
					result = "<span class='glyphicon glyphicon-time alert-warning'></span> " + res.status;
					break;
				default:
					break;

			}
	    document.getElementById("view_mbs_status").innerHTML = result;

			var loans = res.loans;
			var str = "";
			for(var i = 0; i < loans.length; i++){
				str += "<tr><td>" + loans[i].loan_id + "</td><td>" + numberWithCommas(loans[i].amount) + "</td><td>" + Math.round(loans[i].rate * 100)/100 + "</td><td>" + loans[i].bank_name + "</td><td>" + loans[i].created_date + "</td></tr>";

			}
	    document.getElementById("mbs_view_loan_list").innerHTML = str;

	  }else{
	    console.error("An Error");
	    
	  }
	}
	
	xhr.open("GET", "http://localhost:7001/DreamWalker/mbs_service?get_mbs_json=" + mbs_id, true);
	
	xhr.send();
};

var manual_loans;

var  manual_mbs_ajax = function(bank_id){
	
	if(XMLHttpRequest){
	  xhr = new XMLHttpRequest();
	}else{
	  xhr  = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xhr.onreadystatechange = function(){
	  if(xhr.readyState != 4) return;
	  
	  if(xhr.status == 200){
	    var res = JSON.parse(xhr.responseText);
			manual_loans = res.loans;
			var loans = res.loans;
			var str = "";
			for(var i = 0; i < loans.length; i++){
				str += "<tr><td><input type='checkbox' name='manual_loan_list' value='" + loans[i].loan_id + "' onclick='update_mbs()'></td>";
				str += "<td>" + loans[i].loan_id + "</td><td>" + loans[i].amount + "</td><td>" + loans[i].rate + "</td><td>" + loans[i].bank_name + "</td><td>" + loans[i].created_date + "</td></tr>";

			}

			if(str == ""){
				str += "<tr><td></td><td></td><td>No Loan Available</td></tr>"
			}

			document.getElementById("manual_loans").innerHTML = str;
			document.getElementById("manual_submit").value = loans[0].bank_id;

	  }else{
	    console.error("An Error");
	    
	  }
	}
	
	xhr.open("GET", "http://localhost:7001/DreamWalker/loan_service?manual_mbs_json=" + bank_id, true);
	
	xhr.send();
};

var update_mbs = function(){
	var checkbox_list = document.getElementsByName("manual_loan_list");
	var sum_amount = 0;
	var sum_rate = 0;
	var count = 0;
	
	for(var i = 0; i < checkbox_list.length; i++){
		if(checkbox_list[i].checked){
			for(var j=0; j < manual_loans.length; j++){
				if(manual_loans[j].loan_id == checkbox_list[i].value){
					sum_amount += manual_loans[j].amount;
					sum_rate += manual_loans[j].rate;
					count++;
				}
			}
		}
		
	}

	if(count > 0){
		
		document.getElementById("manual_mbs_num_loans").value = count;
		document.getElementById("manual_mbs_sum_amount").value = sum_amount;
		document.getElementById("manual_mbs_avg_rate").value = sum_rate/count;
		
	}
	else{
		
		document.getElementById("manual_mbs_num_loans").value = 0;
		document.getElementById("manual_mbs_sum_amount").value = 0;
		document.getElementById("manual_mbs_avg_rate").value = 0;
	}

	if(sum_amount >= 3000000){
		document.getElementById("manual_submit").removeAttribute("disabled");
	}
	else{
		document.getElementById("manual_submit").setAttribute("disabled", true);
	}

};

var available_loans;
var added_loans;

var populate_loans = function(){
	var str = "";
	for(var i = 0; i < added_loans.length; i++){
		str += "<tr><td>" + added_loans[i].loan_id + "</td><td>" + numberWithCommas(added_loans[i].amount) + "</td><td>" + Math.round(added_loans[i].rate*100)/100 + "</td>";
		str += "<td><button id='added_loanss' name='"+added_loans[i].loan_id+ "' class='btn btn-primary btn-sm' onclick='remove_loan("+ added_loans[i].loan_id +")'>remove</button></td></tr>";

	}
	document.getElementById("added_loans").innerHTML = str;

	str = "";
	for(var i = 0; i < available_loans.length; i++){
		str += "<tr><td>" + available_loans[i].loan_id + "</td><td>" + numberWithCommas(available_loans[i].amount) + "</td><td>" + Math.round(available_loans[i].rate*100)/100 + "</td>";
		str += "<td><button id='available_loanss' name='" + available_loans[i].loan_id + "' class='btn btn-primary btn-sm' onclick='add_loan(" + available_loans[i].loan_id + ")'>add</button></td></tr>";

	}
	if(str != "")
		document.getElementById("available_loans").innerHTML = str;
	else
		document.getElementById("available_loans").innerHTML = "<tr><td></td><td>No loan available</td></tr>";
};

var update_denied_mbs = function(){
	var sum = 0;
	var avg = 0;
	for(var i =0; i < added_loans.length; i++){
		sum += added_loans[i].amount;
		avg += added_loans[i].rate;
	}

	if(added_loans.length > 0){
		avg = avg/added_loans.length;
		document.getElementById("denied_mbs_num_loans").value = added_loans.length;
		document.getElementById("denied_mbs_sum_amount").value = sum;
		document.getElementById("denied-mbs-sum-amount").value = numberWithCommas(sum);
		document.getElementById("denied_mbs_avg_rate").value = avg;
		document.getElementById("denied-mbs-avg-rate").value = Math.round(avg*100)/100;
		
		if(sum >= 3000000){
			document.getElementById("denied_mbs_submit").removeAttribute("disabled");
		}
		else{
			document.getElementById("denied_mbs_submit").setAttribute("disabled", true);
		}
	}
	else{
		document.getElementById("denied_mbs_num_loans").value = 0;
		document.getElementById("denied_mbs_sum_amount").value = 0;
		document.getElementById("denied_mbs_avg_rate").value = 0;
		document.getElementById("denied_mbs_submit").setAttribute("disabled", true);
	}

};

var remove_loan = function(loan_id){
	document.getElementById("loan" + loan_id).removeAttribute("checked");
	var removed_loan;
	var removed_pos;
	for(var i=0; i< added_loans.length; i++){
		if(added_loans[i].loan_id == loan_id){
			removed_loan = added_loans[i];
			removed_pos = i;
		}
	}
	available_loans.push(removed_loan);
	added_loans.splice(removed_pos, 1);
	populate_loans();
	update_denied_mbs();
};

var add_loan = function(loan_id){
	document.getElementById("loan" + loan_id).setAttribute("checked", true);
	var added_loan;
	var removed_pos;
	for(var i=0; i< available_loans.length; i++){
		if(available_loans[i].loan_id == loan_id){
			added_loan = available_loans[i];
			removed_pos = i;
		}
	}
	added_loans.push(added_loan);
	available_loans.splice(removed_pos, 1);
	populate_loans();
	update_denied_mbs();
};

var denied_mbs_json = function(mbs_id){
	if(document.getElementById("denied_row").getAttribute("class") != "row")
		document.getElementById("denied_row").setAttribute("class", "row");

	document.getElementById("Freddie Mac").removeAttribute("selected");
	document.getElementById("Fannie Mae").removeAttribute("selected");
	document.getElementById("Wells Fargo").removeAttribute("selected");
	if(XMLHttpRequest){
	  xhr = new XMLHttpRequest();
	}else{
	  xhr  = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xhr.onreadystatechange = function(){
	  if(xhr.readyState != 4) return;
	  
	  if(xhr.status == 200){
	    var res = JSON.parse(xhr.responseText);
			available_loans = res.available_loans;
			added_loans = res.loans;
			document.getElementById("denied_mbs_id").value = res.mbs_id;
			
			document.getElementById(res.SMO).setAttribute("selected", true);
			document.getElementById("denied_mbs_reason").value = res.denial_reason;
			document.getElementById("denied_mbs_submit").value = res.mbs_id;
			document.getElementById("denied_mbs_save").value = res.mbs_id;

			var hid = "";
			for(var i = 0;i <added_loans.length; i++){
				hid += "<input id='loan"+ added_loans[i].loan_id +"' type='checkbox' hidden checked name='chosen_loans' value='"+ added_loans[i].loan_id +"'/>";
			}

			for(var i = 0; i < available_loans.length; i++){
				hid += "<input id='loan"+ available_loans[i].loan_id +"' type='checkbox' hidden name='chosen_loans' value='"+ available_loans[i].loan_id +"'/>";
			}
			document.getElementById("hidden_loans").innerHTML = hid;

			populate_loans();
			update_denied_mbs();
			
	  }else{
	    console.error("An Error");
	    
	  }
	}
	
	xhr.open("GET", "http://localhost:7001/DreamWalker/mbs_service?denied_mbs_json=" + mbs_id, true);
	
	xhr.send();
};

