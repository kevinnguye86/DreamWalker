package com.ex.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ex.Util.ConnFactory;

public class BankDAO {
	
	private ConnFactory connFactory = ConnFactory.getConnFactory();
	
	
	private static final String GET_NAMES = "select bank_name from Bank";
	private static final String GET_PC ="select payee_code from Bank where bank_id=?";
	
	
	
	public List<String> GetNames()
	{
		List<String> names = new ArrayList<String>();
		
		try(Connection conn = connFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(GET_NAMES);)
		{
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next())
				names.add(rs.getString(1));	
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return names;
				
	}
	
	public String GetPC()
	{
		String PC="";
		
		try(Connection conn = connFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(GET_PC);)
		{
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			PC=rs.getString(1);
				
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return PC;
				
	}
	
	
	

}
