package com.ex.DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ex.Util.ConnFactory;
import com.ex.pojos.Loan;

public class LoanDAO {
	
	private ConnFactory connFactory = ConnFactory.getConnFactory();
	
	// queries
	
	private static final String ALL_LOANS = "select * from loans";
	private static final String THIS_BANKS_LOANS = "select * from loans where bank_id=?";
	private static final String NEW_LOAN = "insert into loans values(?,?,?,?,null)";
	private static final String EDIT_LOAN = "update loans set amount=?, rate=?, bank_id=?, mbs_id=? where loan_id=?";
	private static final String DELETE_LOAN = "delete from loans where loan_id=?";
	private static final String THIS_LOAN = "select * from loans where loan_id=?";
	
	
	public List<Loan> GetLoans()
	{
		List<Loan> loans = new ArrayList<Loan>();
		try(Connection conn = connFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(ALL_LOANS);)
		{
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				Loan l = new Loan();
				l.setLoan_id(rs.getInt(1));
				l.setAmount(rs.getInt(2));
				l.setRate(rs.getInt(3));
				l.setBank_id(rs.getInt(4));
				l.setCreated_date(rs.getDate(5));
				l.setMbs_id(rs.getInt(6));
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return loans;			
	}
	
	
	public List<Loan> BanksLoans(int bank_id)
	{
		List<Loan> loans = new ArrayList<Loan>();
		try(Connection conn = connFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(THIS_BANKS_LOANS);)
		{
			pstmt.setInt(1, bank_id);
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				Loan l = new Loan();
				l.setLoan_id(rs.getInt(1));
				l.setAmount(rs.getInt(2));
				l.setRate(rs.getInt(3));
				l.setBank_id(rs.getInt(4));
				l.setCreated_date(rs.getDate(5));
				l.setMbs_id(rs.getInt(6));
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return loans;			
	}
	
	public void NewLoan(int amount, int rate, int bank_id)
	{
		List<Loan> loans = new ArrayList<Loan>();
		try(Connection conn = connFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(NEW_LOAN);)
		{
			pstmt.setInt(1, amount);
			pstmt.setInt(2, rate);
			pstmt.setInt(3, bank_id);
			pstmt.setDate(4, new Date(System.currentTimeMillis()));
			pstmt.executeUpdate();
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
					
	}
	
	
	public void EditLoan(int amount, int rate, int bank_id, int mbs_id, int loan_id)
	{
		List<Loan> loans = new ArrayList<Loan>();
		try(Connection conn = connFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(EDIT_LOAN);)
		{
			pstmt.setInt(1, amount);
			pstmt.setInt(2, rate);
			pstmt.setInt(3, bank_id);
			pstmt.setInt(4, mbs_id);
			pstmt.setInt(5, loan_id);
			pstmt.executeUpdate();
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
					
	}
	
	public void DeleteLoan(int loan_id)
	{
		List<Loan> loans = new ArrayList<Loan>();
		try(Connection conn = connFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(DELETE_LOAN);)
		{
			
			pstmt.setInt(1, loan_id);
			pstmt.executeUpdate();
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
					
	}
	
	public Loan ThisLoan()
	{
		Loan l = new Loan();
		try(Connection conn = connFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(THIS_LOAN);)
		{
			ResultSet rs = pstmt.executeQuery();
			
			rs.next();
				
				l.setLoan_id(rs.getInt(1));
				l.setAmount(rs.getInt(2));
				l.setRate(rs.getInt(3));
				l.setBank_id(rs.getInt(4));
				l.setCreated_date(rs.getDate(5));
				l.setMbs_id(rs.getInt(6));
				
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return l;
	}
	
	
	
}
