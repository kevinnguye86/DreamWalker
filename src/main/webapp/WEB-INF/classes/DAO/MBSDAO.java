package com.ex.DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ex.Util.ConnFactory;
import com.ex.pojos.MBS;

public class MBSDAO {
	
	private ConnFactory connFactory = ConnFactory.getConnFactory();
	
	// here is where the queries will go.
	
	private static final String MBS_SUM_AMOUNT = "update MBS Set sum_amount =(Select sum(amount) from Loan where mbs_id=?)where mbs_id=?";
	private static final String MBS_AVG_RATE ="update MBS Set avg_rate =(Select avg(rate) from Loan where mbs_id=?)where mbs_id=?";
	private static final String MBS_NUM_LOANS = "update MBS Set num_loans =(Select count(*) from Loan where mbs_id=?)where mbs_id=?";
	private static final String MBS_APPROVED = "update MBS set status='approved', approved_date=? where mbs_id=?";
	private static final String MBS_DENIED = "update MBS set status='open', denial_reason=? where mbs_id=?";
	private static final String MBS_SUBMIT = "update MBS set status='pending' where mbs_id=?";
	private static final String MBS_NEW = "insert into MBS values(?,0,0,0,?,null,'open',null)";
	private static final String MBS_UPDATE = "update MBS set SMO=?, bank_id=?";
	private static final String ALL_MBS = "select * from MBS";
	private static final String ALL_MBS_WITH_STATUS = "select * from MBS where status=?";
	private static final String THIS_MBS ="select * from MBS where mbs_id=?";
	
	
	
	public List<MBS> AllMBS()
	{
		List<MBS> mbs = new ArrayList<MBS>();
		try(Connection conn = connFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(ALL_MBS);)
		{
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				MBS m = new MBS();
				m.setMbs_id(rs.getInt(1));
				m.setBank_id(rs.getInt(2));
				m.setSum_amount(rs.getInt(3));
				m.setAvg_rate(rs.getInt(4));
				m.setNum_loans(rs.getInt(5));
				m.setSMO(rs.getString(6));
				m.setApproved_date(rs.getDate(7));
				m.setStatus(rs.getString(8));
				m.setDenial_reason(rs.getString(9));
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mbs;			
	}
	
	
	public List<MBS> StatusMBS(String status)
	{
		List<MBS> mbs = new ArrayList<MBS>();
		try(Connection conn = connFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(ALL_MBS_WITH_STATUS);)
		{
			pstmt.setString(1, status);
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				MBS m = new MBS();
				m.setMbs_id(rs.getInt(1));
				m.setBank_id(rs.getInt(2));
				m.setSum_amount(rs.getInt(3));
				m.setAvg_rate(rs.getInt(4));
				m.setNum_loans(rs.getInt(5));
				m.setSMO(rs.getString(6));
				m.setApproved_date(rs.getDate(7));
				m.setStatus(rs.getString(8));
				m.setDenial_reason(rs.getString(9));
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mbs;			
	}

	
	public MBS ThisMBS()
	{
		MBS m = new MBS();
		try(Connection conn = connFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(THIS_MBS);)
		{
			ResultSet rs = pstmt.executeQuery();
			
			rs.next();
				
				m.setMbs_id(rs.getInt(1));
				m.setBank_id(rs.getInt(2));
				m.setSum_amount(rs.getInt(3));
				m.setAvg_rate(rs.getInt(4));
				m.setNum_loans(rs.getInt(5));
				m.setSMO(rs.getString(6));
				m.setApproved_date(rs.getDate(7));
				m.setStatus(rs.getString(8));
				m.setDenial_reason(rs.getString(9));
				
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return m;			
	}

	public void Sum(int mbs_id)
	{
		
		try(Connection conn = connFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(MBS_SUM_AMOUNT);)
		{
			pstmt.setInt(1, mbs_id);
			pstmt.setInt(2, mbs_id);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
					
	}
	
	public void Rate(int mbs_id)
	{
		
		try(Connection conn = connFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(MBS_AVG_RATE);)
		{
			pstmt.setInt(1, mbs_id);
			pstmt.setInt(2, mbs_id);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
					
	}
	
	public void NumLoans(int mbs_id)
	{
		
		try(Connection conn = connFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(MBS_NUM_LOANS);)
		{
			pstmt.setInt(1, mbs_id);
			pstmt.setInt(2, mbs_id);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
					
	}
	
	public void Approved(int mbs_id)
	{
		
		try(Connection conn = connFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(MBS_APPROVED);)
		{
			pstmt.setDate(1, new Date(System.currentTimeMillis()));
			pstmt.setInt(2, mbs_id);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
					
	}
	
	public void Denied(String denial_reason, int mbs_id)
	{
		
		try(Connection conn = connFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(MBS_DENIED);)
		{
			pstmt.setString(1,denial_reason);
			pstmt.setInt(2, mbs_id);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
					
	}
	
	public void Submit(int mbs_id)
	{
		
		try(Connection conn = connFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(MBS_SUBMIT);)
		{
			
			pstmt.setInt(1, mbs_id);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
					
	}
	
	
	public void NewMBS(int bank_id, String SMO)
	{
		
		try(Connection conn = connFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(MBS_NEW);)
		{
			pstmt.setInt(1, bank_id);
			pstmt.setString(2, SMO);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
					
	}
	
	public void NewMBS( String SMO, int bank_id)
	{
		
		try(Connection conn = connFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(MBS_UPDATE);)
		{
			pstmt.setString(1, SMO);
			pstmt.setInt(2, bank_id);
			
			pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
					
	}
	
	
	
}
