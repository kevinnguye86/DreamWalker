package com.ex.pojos;

public class Bank {
	
	private int bank_id;
	private String name;
	private String payee_code;
	
	
	
	
	public int getBank_id() {
		return bank_id;
	}
	public void setBank_id(int bank_id) {
		this.bank_id = bank_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPayee_code() {
		return payee_code;
	}
	public void setPayee_code(String payee_code) {
		this.payee_code = payee_code;
	}
	
	
	
	@Override
	public String toString() {
		return "Bank [bank_id=" + bank_id + ", name=" + name + ", payee_code=" + payee_code + "]";
	}
	
	

}
