package com.ex.pojos;


import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import com.ex.DAO.LoanDAO;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class MBS {
	
	private int mbs_id;
	private String bank_name;
	private double sum_amount;
	private double avg_rate;
	private int num_loans;
	private String SMO;
	private Date approved_date;
	private String status;
	private String denial_reason;
	private int bank_id;
	private List<Loan> loans;
	
	public JsonObject toJSON(){
		SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy");
		JsonObject mbs = new JsonObject();
		JsonObject loan;
		JsonArray loans = new JsonArray();
		
		mbs.addProperty("mbs_id", this.mbs_id);
		mbs.addProperty("bank_name", this.bank_name);
		mbs.addProperty("sum_amount", this.sum_amount);
		mbs.addProperty("avg_rate", this.avg_rate);
		mbs.addProperty("num_loans", this.num_loans);
		mbs.addProperty("SMO", this.SMO);
		if(this.approved_date != null)
			mbs.addProperty("approved_date", sdf.format(this.approved_date));
		else mbs.addProperty("approved_date","");
		mbs.addProperty("status", this.status);
		mbs.addProperty("denial_reason", this.denial_reason);
		mbs.addProperty("bank_id", this.bank_id);
		
		for(Loan l: this.loans){
			loans.add(l.toJSON());
		}
		
		mbs.add("loans", loans);
		
		return mbs;
		
	}
	
	public List<Loan> getLoans() {
		return loans;
	}

	public void setLoans(List<Loan> loans) {
		this.loans = loans;
	}

	public int getBank_id() {
		return bank_id;
	}
	public void setBank_id(int bank_id) {
		this.bank_id = bank_id;
	}
	public int getMbs_id() {
		return mbs_id;
	}
	public void setMbs_id(int mbs_id) {
		this.mbs_id = mbs_id;
		//this.loans = LoanDAO.MBSLoans(this.mbs_id);
	}
	public String getBank_name() {
		return bank_name;
	}
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	public double getSum_amount() {
		return sum_amount;
	}
	public void setSum_amount(double sum_amount) {
		this.sum_amount = sum_amount;
	}
	public double getAvg_rate() {
		return avg_rate;
	}
	public void setAvg_rate(double avg_rate) {
		this.avg_rate = avg_rate;
	}
	public int getNum_loans() {
		return num_loans;
	}
	public void setNum_loans(int num_loans) {
		this.num_loans = num_loans;
	}
	public String getSMO() {
		return SMO;
	}
	public void setSMO(String sMO) {
		SMO = sMO;
	}
	public Date getApproved_date() {
		return approved_date;
	}
	public void setApproved_date(Date approved_date) {
		this.approved_date = approved_date;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDenial_reason() {
		return denial_reason;
	}
	public void setDenial_reason(String denial_reason) {
		this.denial_reason = denial_reason;
	}
	
	
	@Override
	public String toString() {
		return "MBS [mbs_id=" + mbs_id + ", bank_id=" + bank_name + ", sum_amount=" + sum_amount + ", avg_rate="
				+ avg_rate + ", num_loans=" + num_loans + ", SMO=" + SMO + ", approved_date=" + approved_date
				+ ", status=" + status + ", denial_reason=" + denial_reason + "]";
	}
	
	

}
