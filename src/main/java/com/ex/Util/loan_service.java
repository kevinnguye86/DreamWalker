package com.ex.Util;


import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ex.DAO.LoanDAO;
import com.ex.DAO.MBSDAO;
import com.ex.pojos.Loan;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


public class loan_service extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public loan_service() {
        super();
    }

    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		System.out.println("");
	}

    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(request.getParameter("get_loan_json") != null){
			int loan_id = Integer.parseInt(request.getParameter("get_loan_json"));
			Loan loan = LoanDAO.ThisLoan(loan_id);
			JsonObject json = loan.toJSON();
			response.setContentType("application/json");
			response.getWriter().write(json.toString());
		}
		
		if(request.getParameter("manual_mbs_json") != null){
			List<Loan> loans = LoanDAO.BanksLoansForMBS(Integer.parseInt(request.getParameter("manual_mbs_json")));
			JsonArray loan_array = new JsonArray();
			JsonObject json = new JsonObject();
			for(Loan l: loans){
				loan_array.add(l.toJSON());
			}
			json.add("loans", loan_array);
			response.setContentType("application/json");
			response.getWriter().write(json.toString());
		}

		if(request.getParameter("submit") != null && request.getParameter("submit").equals("create_loan")) {
			
			double amount = Double.parseDouble(request.getParameter("amount"));
			double interest_rate = Double.parseDouble(request.getParameter("interest_rate"));
			int bank_id = Integer.parseInt(request.getParameter("bank"));
			int loan_id = LoanDAO.NewLoan(amount, interest_rate, bank_id);
			response.sendRedirect("bank.jsp?created=" + loan_id);
		}
		
		if(request.getParameter("commit_edit_loan") != null){
			LoanDAO.EditLoan(Double.parseDouble(request.getParameter("amount")),
					Double.parseDouble(request.getParameter("interest_rate")), 
					Integer.parseInt(request.getParameter("bank")),
					Integer.parseInt(request.getParameter("commit_edit_loan")));
			response.sendRedirect("bank.jsp?edited="+request.getParameter("commit_edit_loan"));
		}
		
		if(request.getParameter("commit_delete_loan") != null){
			LoanDAO.DeleteLoan(Integer.parseInt(request.getParameter("commit_delete_loan")));
			response.sendRedirect("bank.jsp?deleted="+request.getParameter("commit_delete_loan"));
		}
		
		
		
	}
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		
	}

}
